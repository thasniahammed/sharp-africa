<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = array('sr_no','date','customer','country','model','serial_no');
}
