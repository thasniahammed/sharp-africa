<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Excelimport extends Model
{
    protected $fillable = array('sr_no','date','customer','country','model','serial_no');

}
