<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Excelimport;
use App\Imports\ExcelImp;
use App\Imports\ProductsImport;

use App\Country;
use Session;
use App\UnverifiedProduct;
use Maatwebsite\Excel\Facades\Excel;

use DB;

class ProductController extends Controller
{
    public function index()
    {

        $products=Product::latest()->get();
       
        return view('admin.products.index',compact('products'));
    }

    public function editProduct($id)
    {
        $countries      =   Country::get();
        $product        =   Product::where('id',$id)->first();

        return view('admin.products.edit',compact('product','countries'));
    }

    public function editProductSubmit(Request $request,$id)
    {

      $product                   =   Product::where('id',$id)->first();
        $product->serial_no             =   $request->get('serial_no');
        $product->model            =   $request->get('model');
        $product->customer         =   $request->get('customer');
        $product->country          =   $request->get('country');
        $product->date           =   $request->get('date');
       
        $product->save();
        Session::flash('message', 'Product Updated successfully');
        //$msg                    =   'Product updated successfully';
        $products                  =   Product::latest()->get();
        return redirect('/admin/products');
    }
    public function store(Request $request)
    {
        if($request->isMethod('post')){

            Product::create([
                'sr_no'         => '#11',
                'serial_no'     => $request->get('serial_no'),
                'model'         => $request->get('model'),
                'customer'      => $request->get('customer'),
                'country'       => $request->get('country'),
                'date'          => $request->get('date'),
                'is_verified'          => '0'
            ]);
            Session::flash('message', 'Product Added successfully');
            return redirect('/admin/products');

        }
        $countries      =   Country::get();
        return view('admin.products.create',compact('countries'));
    }

    public function delete($id)
    {
        $product    = Product::where('id', $id)->delete();  
        Session::flash('message', 'Product Delete successfully');
        return redirect('/admin/products');
    }
    public function unverifiedProducts()
    {
        $products=UnverifiedProduct::latest()->get();
       
        return view('admin.unverifiedproducts.index',compact('products'));
    }
    
    public function deleteUnverify($id)
    {
        $product    = UnverifiedProduct::where('id', $id)->delete();  
        Session::flash('message', 'Unverified Product Deleted');
        return redirect('/admin/un-verify-products');
    }
    public function deleteAll(Request $request)
    {
        
        //$productIds=array();
        $str_arr=array();
            $productIds=$request->get('id');
           // print_r($productIds);exit();
           $str_arr = explode (",", $productIds);
          
            $j=0;
            for($i=1;$i<=count($str_arr);$i=$i+1){
                
              $product    = UnverifiedProduct::where('id', $str_arr[$j])->delete();  
              
                $j=$j+1;
            }
            $url="/admin/un-verify-products";
            return $url;
            
    }
    
    public function deleteAllproduct(Request $request)
    {
       
        //$productIds=array();
        $str_arr=array();
            $productIds=$request->get('id');
           // print_r($productIds);exit();
           $str_arr = explode (",", $productIds);
          
            $j=0;
            for($i=1;$i<=count($str_arr);$i=$i+1){
                
              $product    = Product::where('id', $str_arr[$j])->delete();  
              
                $j=$j+1;
            }
            // $url="/admin/products";
            // return $url;
            
    }
    public function dataImport()
    {
        $products=Product::latest()->get();
      
        return view('admin.products.data-import',compact('products'));

    }
    public  function dataImportS(Request $request)
    {
     
               
            //  $this->validate($request, [
            //   'select_file'  => 'required|mimes:xls,xlsx,csv'
            //  ]);

            //pradeep code
            $file= request()->file('select_file');
            //dd($file);exit;
            Excel::import(new ProductsImport, $file);
            return redirect('/admin/dataimport')->with('flash_message_success', 'New Products Uploaded Successfully.');
            //pradeep code end 
    
           // $data = Excel::load($path)->get();
            // $serial_nos = Product::lists('sn')->toArray();
            // $serial_nos = DB::table('products')->pluck('sn','id')->toArray();
            //$serial_nos = Product::select('serial_no', 'id')->get();
           // $serial_nos = collect($serial_nos->toArray())->flatten()->all();
              // print_r($data);exit;
            //  if($data->count() > 0)
            //  {
            //         // DB::table('products')->delete();
            //         $i=0;
            //       foreach($data as $key => $value)
            //       {
            //        // print_r(count($value));exit('guu');
            //            foreach($value as $row)
            //            {
            //               // print_r($row);exit;
            //             if($row){
            //                 if (in_array($row['serial_no'], $serial_nos))
            //                 {
            //                     echo $row['sr_no'];
            //                     echo '-'.$row['0'];
            //                     echo '-'.$row['month'];exit;
            //                     $update_data[] = array(
            //                         'sr_no'  => $row['sr_no'],
            //                         'month'   => $row['month'],
            //                         'date'   => $row['date'],
            //                         'customer'   => $row['customer'],
            //                         'country'   => $row['country'],
            //                         'model'   => $row['model'],
            //                         'serial_no'   => $row['serial_no'],
            //                         'is_verified' => '0',
            //                         'is_updated' => '1',
                                
            //                     );
                                
                                    
                             

            //                 }else{

            //                     $insert_data[] = array(
            //                         'sr_no'  => $row['sr_no'],
            //                         'month'   => $row['month'],
            //                         'date'   => $row['date'],
            //                         'customer'   => $row['customer'],
            //                         'country'   => $row['country'],
            //                         'model'   => $row['model'],
            //                         'serial_no'   => $row['serial_no'],
            //                         'is_verified' => '0',
            //                         'is_updated' => '0',
                                
            //                     );
                                      
                                        
                                    
            //                 }
            //                 $i=$i+1;
            //             }
                       
                        
            //         }
                   

            //       }
                 
    //print_r($insert_data);exit;
                //   if(!empty($insert_data))
                //   {
                //    DB::table('products')->insert($insert_data);
                //   }
                
                //   if(!empty($update_data))
                //   {
                //     DB::table('products')->whereIn('serial_no', $update_data['sn'])->update($update_data);
                //   }
                 
            // }
           // return redirect('/admin/dataimport')->with('flash_message_success', 'New Products Uploaded Successfully.');
    
            
    
            
    } 

}
