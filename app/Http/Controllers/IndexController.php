<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Product;
use App\UnverifiedProduct;

class IndexController extends Controller
{
    public function index()
    {
        $countries=Country::get();
        
        return view('index',compact('countries'));
    }
    public function searchproduct(Request $request)
    {
        //print_r($request->all());exit();
        $country_name=$request->get('country');
        $serial_no=$request->get('serial_no');
        $products=Product::where('country','=',$country_name)
                        ->where('serial_no','=',$serial_no)->first();
       // $products=Product::where('serial_no','=',$serial_no)->first();
        if($products){
             //updating Is verified field 
             $products->is_verified=1;
             $products->ip_address=\Request::ip();
             $products->save();
             //ends
        }else{
            //inserting Un verified table 
            UnverifiedProduct::create([

                'country'        => $request->get('country'),
                'model'         => 'model',
                'serial_no'     => $request->get('serial_no'),
                'ip_address'    => \Request::ip(),
               
            ]);

            //ends 
        }          
        return response()->json(['data'=>$products,'flash_message_success'=>'Data  successfully found!!']);
    }
    public function multiples()
    {
        $countries=Country::get();
       
        return view('multiple-products',compact('countries'));
    }
    
    public function multipleproductsubmit(Request $request)
    {
        //print_r($request->all());exit();
      
        $countryLength=count($request->country);
        $serialNoLength=count($request->serial_no);

        $country=$request->country;
        $serialNo=$request->serial_no;

        if($countryLength>=$serialNoLength){

            $totalLen=$countryLength;

        }else{

            $totalLen=$serialNoLength;
        }
        
        $tempArr=array();
        $xcount=0;
        $ycount=0;
        $result['allok']='';
        $result['allnotok']            =   '';
        for($i=0;$i<$totalLen;){
            

            $result['item']                 =   $i;
            $result['serial_no']            =   $serialNo[$i];
            $result['country']              =   $country[$i];
            $products=Product::where('country','=',$country[$i])
                         ->where('serial_no','=',$serialNo[$i])->first();
            //$products=Product::where('serial_no','=',$serialNo[$i])->first();
            if($products){
                $result['model']           =   $products['model'];
                $result['customer']        =   $products['customer'];
                $result['date']            =   $products['date'];
                $result['allok']            =   $xcount+1;
                $xcount=$xcount+1;
                //updating Is verified field 
                $products->is_verified=1;
                $products->ip_address=\Request::ip();
                $products->save();
                //ends
            }else{
                $result['model']           =   $products['model'];
                $result['customer']        =   $products['customer'];
                $result['date']            =   $products['date'];
                $result['allnotok']            =   $ycount+1;
                $ycount=$ycount+1;
                //inserting Un verified table 
                UnverifiedProduct::create([

                    'country'        => $country[$i],
                    'model'         => 'model',
                    'serial_no'      => $serialNo[$i],
                    'ip_address'    => \Request::ip(),
                   
                ]);

                //ends
            }
           
            $i=$i+1;
            array_push($tempArr,$result);

        }
        //print_r($result);exit;
        if($totalLen==$result['allok']){
            $status=1;
                
        }elseif ($totalLen==$result['allnotok']){

            $status=2;
            
        }else{
            $status=3;
        }
       
        return response()->json(['dataArr'=>$tempArr,'status'=>$status,'flash_message_success'=>'Data  successfully found!!']); 
        //return view('result-products',compact('countries','tempArr'));
    
    
    }
}
