<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;
use DB;

use Illuminate\Database\Eloquent\Model;

class AdminController extends Controller
{ 
    
    public function login(Request  $request){
    	if($request->isMethod('post')){
    		$data= $request->input();
    		if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password'],'is_admin'=>'1'])){
    			return redirect('admin/products');
    		}
    		else{
    			return redirect('/admin')->with('flash_message_error','invalid username or password');
    		}
    	}
    	return view('admin.admin_login');
    }
    public function dashboard(){
    	return view('admin.dashboard');
    }
    public function logout(){
        Session::flush();
		return redirect('/admin')->with('flash_message_success','Logged out successfully');
    }

    public function users()
    {

        $users=User::where('name','!=','admin')->where('is_admin','=','0')->get();
        $msg='';
        return view('admin.users.index',compact('users','msg'));
    }
    public function editUser($id)
    {
        $user=User::where('id',$id)->first();

        return view('admin.users.edit',compact('user'));
    }

    public function editUserSubmit(Request $request,$id)
    {

      
        //     $this->validate($request, [

        //     'name'          => 'required',
        //     'gender'        => 'required',
        //     'phone_no'      => 'required',
        //     'state'         => 'required',
        //     'address'         => 'required',
        //     'zipcode'         => 'required',
        //     'city'         => 'required',
        // ]);
        
        $user                   =   User::where('id',$id)->first();
        $user->name             =   $request->get('name');
        $user->email            =   $request->get('email');
        $user->phone_no         =   $request->get('phone_no');
        $user->address          =   trim($request->get('address'));
        $user->gender           =   $request->get('gender');
        $user->dob             =   $request->get('dateofbirth');
        $user->marital_status   =   $request->get('marital_status');
        $user->state            =   $request->get('state');
        $user->zipcode          =   $request->get('zipcode');
        $user->city             =   $request->get('city');
        $user->is_profile       =   1;
        $user->save();

        $msg                    =   'Profile updated successfully';
        $users                  =   User::where('name','!=','admin')->where('status','=','1')->get();
        return view('admin.users.index',compact('users','msg'));
    }

    public function delete($id){


        $user=User::find($id);

        if($user->status=='1'){

             $user->status=0;
            $user->save();
            $msg                    =   'User deleted successfully';

        }else{

            $msg                    =   'User Already deleted';
       
        }
       
       $users                  =   User::where('name','!=','admin')->where('status','=','1')
                                    ->get();
    return view('admin.users.index',compact('users','msg'));
        

    }

}
