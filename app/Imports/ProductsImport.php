<?php

namespace App\Imports;

use App\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Throwable;

class ProductsImport implements ToModel,WithHeadingRow ,SkipsOnError
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        if(isset($row['sr_no']) && isset($row['date'])&& isset($row['month'])&& isset($row['customer'])&& isset($row['country'])&& isset($row['model'])&& isset($row['serial_no'])){
           // print_r($row);
            $date=str_split($row['date'], 2);
            $newdate = date_format(date_create($date[0].'-'.$date[1].'-'.$date[0]),"Y-m-d");
             return new Product([
            'sr_no' => $row['sr_no'],
            'date' =>  $newdate,
            'customer' => $row['customer'],
            'country' => $row['country'],
            'model' => $row['model'],
            'serial_no' => $row['serial_no']
            ]);
        }
        
    }
    public function onError(Throwable $error){
       // print_r($error);

    }
}
