<?php

namespace App\Imports;

use App\Excelimport;
use Maatwebsite\Excel\Concerns\ToModel;

class ExcelImp implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $date=str_split($row[1], 2);
        $newdate = date_format(date_create($date[0].'-'.$date[1].'-'.$date[0]),"Y-m-d");
        return new Excelimport([
            'sr_no' => $row[0],
            'date' =>  $newdate,
            'customer' => $row[2],
            'country' => $row[3],
            'model' => $row[4],
            'serial_no' => $row[5],
        ]);
    }
}
