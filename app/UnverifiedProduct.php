<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnverifiedProduct extends Model
{
    protected $fillable = [
        'country','model','sn','ip_address'
    ];
}
