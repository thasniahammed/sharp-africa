<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'IndexController@index')->name('index');
Route::post('/frmsharpsubmit', 'IndexController@searchproduct');
Route::get('/multiple-products', 'IndexController@multiples');
Route::post('/multipleSubmit', 'IndexController@multipleproductsubmit');
Route::match(['get','post'],'/admin','AdminController@login');
Route::match(['get','post'],'/admin/login','AdminController@login');
Route::group(['middleware'=>['auth']],function(){

    Route::get('/admin/dashboard','AdminController@dashboard');
    //users
    Route::get('/admin/users','AdminController@users');
	Route::get('/admin/edit-user/{id}','AdminController@editUser');
	Route::post('/admin/edit-user-submit/{id}','AdminController@editUserSubmit');
    Route::get('/admin/delete-user/{id}','AdminController@delete');
    
    //products
    Route::get('/admin/products','ProductController@index');
    Route::match(['get','post'],'/admin/add-product','ProductController@store');
	Route::get('/admin/edit-product/{id}','ProductController@editProduct');
	Route::post('/admin/edit-product-submit/{id}','ProductController@editProductSubmit');
    Route::get('/admin/delete-product/{id}','ProductController@delete');
    Route::get('/admin/un-verify-products','ProductController@unverifiedProducts');
    Route::get('/admin/delete-un-vproduct/{id}','ProductController@deleteUnverify');
    Route::post('admin/deleteAll','ProductController@deleteAll');
    Route::post('admin/deleteAllproduct','ProductController@deleteAllproduct');
    //Data Import
    Route::get('admin/dataimport','ProductController@dataImport');
    Route::post('/admin/importdata-post', 'ProductController@dataImportS');
    
    
});
Route::get('/admin/logout','AdminController@logout');
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Clear Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

