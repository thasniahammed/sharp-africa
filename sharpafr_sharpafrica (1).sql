-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2020 at 07:01 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sharpafr_sharpafrica`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `SR_` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `COUNTRIES` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `SR_`, `COUNTRIES`, `created_at`, `updated_at`) VALUES
(1, '1', 'AFGHANISTAN', NULL, NULL),
(2, '2', 'ANGOLA', NULL, NULL),
(3, '3', 'BENIN', NULL, NULL),
(4, '4', 'BOTSWANA', NULL, NULL),
(5, '5', 'BURKINA FASO', NULL, NULL),
(6, '6', 'BURUNDI', NULL, NULL),
(7, '7', 'CAMEROON', NULL, NULL),
(8, '8', 'COMOROS', NULL, NULL),
(9, '9', 'DRC CONGO', NULL, NULL),
(10, '10', 'DJIBOUTI', NULL, NULL),
(11, '11', 'ERITREA', NULL, NULL),
(12, '12', 'ETHIOPIA', NULL, NULL),
(13, '13', 'GHANA', NULL, NULL),
(14, '14', 'IRAQ', NULL, NULL),
(15, '15', 'IVORY COAST', NULL, NULL),
(16, '16', 'KENYA', NULL, NULL),
(17, '17', 'LIBYA', NULL, NULL),
(18, '18', 'MADAGASCAR', NULL, NULL),
(19, '19', 'MALAWI', NULL, NULL),
(20, '20', 'MAURITANIA', NULL, NULL),
(21, '21', 'MAURITIUS', NULL, NULL),
(22, '22', 'MOZAMBIQUE', NULL, NULL),
(23, '23', 'NIGERIA', NULL, NULL),
(24, '24', 'SENEGAL', NULL, NULL),
(25, '25', 'SEYCHELLES', NULL, NULL),
(26, '26', 'SOMALIA', NULL, NULL),
(27, '27', 'SOUTH SUDAN', NULL, NULL),
(28, '28', 'SUDAN', NULL, NULL),
(29, '29', 'TANZANIA', NULL, NULL),
(30, '30', 'TOGO', NULL, NULL),
(31, '31', 'YEMEN', NULL, NULL),
(32, '32', 'ZAMBIA', NULL, NULL),
(33, '33', 'ZIMBABWE', NULL, NULL),
(34, '34', 'UAE - Export', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2019_11_05_115634_create_products_table', 3),
(7, '2019_11_05_104409_create_countrys_table', 4),
(8, '2019_11_07_115323_add_is_admin_to_users_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sr_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `month` varchar(122) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_verified` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_updated` int(11) NOT NULL DEFAULT 0,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sr_no`, `month`, `date`, `customer`, `country`, `model`, `sn`, `is_verified`, `is_updated`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, '1', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'MX-2010U', '43002350', '0', 0, '0', NULL, NULL),
(2, '2', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'MX-2010U', '43002510', '0', 0, '0', NULL, NULL),
(3, '3', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'MX-2010U', '43002520', '0', 0, '0', NULL, NULL),
(4, '4', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'MX-2314N', '43003140', '0', 0, '0', NULL, NULL),
(5, '5', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'MX-2314N', '43003150', '0', 0, '0', NULL, NULL),
(6, '6', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'MX-2314N', '43002790', '0', 0, '0', NULL, NULL),
(7, '7', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'MX-2314N', '43002960', '0', 0, '0', NULL, NULL),
(8, '8', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'MX-2314N', '43002840', '0', 0, '0', NULL, NULL),
(9, '9', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'MX-2314N', '43002757', '0', 0, '0', NULL, NULL),
(10, '10', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'MX-2314N', '43002780', '0', 0, '0', NULL, NULL),
(11, '11', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032847', '0', 0, '0', NULL, NULL),
(12, '12', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43063827', '0', 0, '0', NULL, NULL),
(13, '13', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032767', '0', 0, '0', NULL, NULL),
(14, '14', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032817', '0', 0, '0', NULL, NULL),
(15, '15', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032737', '0', 0, '0', NULL, NULL),
(16, '16', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032397', '0', 0, '0', NULL, NULL),
(17, '17', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032457', '0', 0, '0', NULL, NULL),
(18, '18', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032777', '0', 0, '0', NULL, NULL),
(19, '19', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032787', '0', 0, '0', NULL, NULL),
(20, '20', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032467', '0', 0, '0', NULL, NULL),
(21, '21', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032857', '0', 0, '0', NULL, NULL),
(22, '22', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032797', '0', 0, '0', NULL, NULL),
(23, '23', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032547', '0', 0, '0', NULL, NULL),
(24, '24', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032927', '0', 0, '0', NULL, NULL),
(25, '25', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032507', '0', 0, '0', NULL, NULL),
(26, '26', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032517', '0', 0, '0', NULL, NULL),
(27, '27', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032427', '0', 0, '0', NULL, NULL),
(28, '28', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032437', '0', 0, '0', NULL, NULL),
(29, '29', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032417', '0', 0, '0', NULL, NULL),
(30, '30', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032447', '0', 0, '0', NULL, NULL),
(31, '31', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032387', '0', 0, '0', NULL, NULL),
(32, '24', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032927', '0', 0, '0', NULL, NULL),
(33, '25', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032507', '0', 0, '0', NULL, NULL),
(37, '26', '2015-01-04 00:00:00', '150104', 'ALTA', 'ETHIOPIA', 'AR-5620VY', '43032517', '0', 0, '0', NULL, NULL),
(38, '234', '2014-01-02 00:00:00', '2432432', 'write', 'sad', 'wer', '233333', '0', 0, '0', NULL, NULL),
(39, '56', '2016-01-02 00:00:00', '35345', 'sdfdsf', 'sfsdf', '345435', '345435435', '0', 0, '0', NULL, NULL),
(40, '56', '2016-01-02 00:00:00', '35345', 'sdfdsf', 'sfsdf', '345435', '64564636', '0', 0, '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `unverified_products`
--

CREATE TABLE `unverified_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `unverified_products`
--

INSERT INTO `unverified_products` (`id`, `country`, `model`, `sn`, `ip_address`, `created_at`, `updated_at`) VALUES
(3, 'MAURITANIA', 'model', '83023130', '117.193.79.2', '2019-11-11 10:43:46', '2019-11-11 10:43:46'),
(7, 'BURKINA FASO', 'model', '8207792 900001', '111.92.78.76', '2019-11-14 03:53:57', '2019-11-14 03:53:57'),
(8, 'LIBYA', 'model', '8207792 900003', '111.92.78.76', '2019-11-14 03:53:57', '2019-11-14 03:53:57'),
(9, 'BURKINA FASO', 'model', '8207792 900001', '111.92.78.76', '2019-11-14 04:06:33', '2019-11-14 04:06:33'),
(10, 'LIBYA', 'model', '8207792 900003', '111.92.78.76', '2019-11-14 04:06:33', '2019-11-14 04:06:33'),
(11, 'AFGHANISTAN', 'model', '123123123123', '111.92.78.76', '2019-11-14 04:22:15', '2019-11-14 04:22:15'),
(12, 'NIGERIA', 'model', '1', '94.204.179.142', '2019-11-17 17:14:21', '2019-11-17 17:14:21'),
(13, 'NIGERIA', 'model', '12345678', '94.204.179.142', '2019-11-17 17:14:32', '2019-11-17 17:14:32'),
(14, 'AFGHANISTAN', 'model', '8307653x', '217.165.139.144', '2019-11-19 10:15:35', '2019-11-19 10:15:35'),
(15, 'AFGHANISTAN', 'model', '93012565', '217.165.139.144', '2019-11-19 10:21:15', '2019-11-19 10:21:15'),
(16, 'AFGHANISTAN', 'model', '93012565', '217.165.139.144', '2019-11-19 10:21:19', '2019-11-19 10:21:19'),
(17, 'DJIBOUTI', 'model', '8207792 900001', '117.193.79.2', '2019-11-20 05:35:29', '2019-11-20 05:35:29'),
(18, 'KENYA', 'model', 'sfsdfsdf', '117.193.79.2', '2019-11-20 05:41:26', '2019-11-20 05:41:26'),
(19, 'MAURITANIA', 'model', 'sdgfdgdfg', '117.193.79.2', '2019-11-20 05:42:22', '2019-11-20 05:42:22'),
(20, 'LIBYA', 'model', '8207792 900001sdfs', '117.193.79.2', '2019-11-20 05:44:16', '2019-11-20 05:44:16'),
(21, 'MADAGASCAR', 'model', 'sfsdf', '117.193.79.2', '2019-11-20 05:44:16', '2019-11-20 05:44:16'),
(22, 'AFGHANISTAN', 'model', '8307653X', '217.165.139.144', '2019-11-24 08:36:44', '2019-11-24 08:36:44'),
(23, 'AFGHANISTAN', 'model', '8457622143', '217.165.139.144', '2019-12-04 10:49:23', '2019-12-04 10:49:23'),
(24, 'AFGHANISTAN', 'model', '83026839', '217.165.139.144', '2019-12-04 10:49:58', '2019-12-04 10:49:58'),
(25, 'AFGHANISTAN', 'model', '83026839', '217.165.139.144', '2019-12-04 10:50:01', '2019-12-04 10:50:01'),
(26, 'BENIN', 'model', '83026839', '217.165.139.144', '2019-12-04 10:50:13', '2019-12-04 10:50:13'),
(27, 'AFGHANISTAN', 'model', '83025579', '217.165.139.144', '2019-12-04 10:50:42', '2019-12-04 10:50:42'),
(28, 'SEYCHELLES', 'model', 'wqerrewrq', '117.193.79.2', '2019-12-17 04:57:54', '2019-12-17 04:57:54'),
(29, 'MADAGASCAR', 'model', 'dfafdf', '117.193.79.2', '2019-12-17 04:58:22', '2019-12-17 04:58:22'),
(30, 'BENIN', 'model', 'asfasfas', '117.193.79.2', '2019-12-17 04:58:22', '2019-12-17 04:58:22'),
(31, 'KENYA', 'model', 'sdfadsfsf', '117.193.79.2', '2019-12-17 04:58:22', '2019-12-17 04:58:22'),
(32, 'LIBYA', 'model', 'fasfds', '117.193.79.2', '2019-12-17 04:58:22', '2019-12-17 04:58:22'),
(33, 'MALAWI', 'model', 'afasdf', '117.193.79.2', '2019-12-17 04:58:22', '2019-12-17 04:58:22'),
(34, 'MADAGASCAR', 'model', '8207792 900001', '117.193.79.2', '2019-12-17 05:57:58', '2019-12-17 05:57:58'),
(35, 'DJIBOUTI', 'model', '1547896541', '217.165.139.144', '2020-01-16 05:49:14', '2020-01-16 05:49:14'),
(36, 'DJIBOUTI', 'model', '83002765', '217.165.139.144', '2020-01-16 05:50:08', '2020-01-16 05:50:08'),
(37, 'SEYCHELLES', 'model', '83002765', '217.165.139.144', '2020-01-16 05:50:19', '2020-01-16 05:50:19'),
(38, 'SEYCHELLES', 'model', '75112941', '217.165.139.144', '2020-01-16 05:50:48', '2020-01-16 05:50:48'),
(39, 'SEYCHELLES', 'model', '75112941', '217.165.139.144', '2020-01-16 05:51:06', '2020-01-16 05:51:06'),
(40, 'MOZAMBIQUE', 'model', '93023042', '217.165.139.144', '2020-01-16 05:51:38', '2020-01-16 05:51:38'),
(41, 'SOMALIA', 'model', '93023042', '217.165.139.144', '2020-01-16 05:51:49', '2020-01-16 05:51:49'),
(42, 'AFGHANISTAN', 'model', '83009955', '217.165.139.144', '2020-01-16 05:55:51', '2020-01-16 05:55:51'),
(43, 'MOZAMBIQUE', 'model', '8302728Y', '217.165.139.144', '2020-01-16 06:12:51', '2020-01-16 06:12:51'),
(44, 'AFGHANISTAN', 'model', '1', '217.165.139.144', '2020-01-16 06:13:58', '2020-01-16 06:13:58'),
(49, 'KENYA', 'model', '8207792 900001', '117.193.79.2', '2020-01-20 04:43:24', '2020-01-20 04:43:24'),
(53, 'GHANA', 'model', '8207792900005', '111.92.78.124', '2020-01-20 10:26:51', '2020-01-20 10:26:51'),
(54, 'IVORY COAST', 'model', 'fsdfsd', '117.193.79.2', '2020-01-22 10:30:30', '2020-01-22 10:30:30'),
(55, 'KENYA', 'model', 'fsdfsd', '117.193.79.2', '2020-01-22 10:30:30', '2020-01-22 10:30:30'),
(56, 'IVORY COAST', 'model', 'sdfsdfd', '117.193.79.2', '2020-01-22 10:30:30', '2020-01-22 10:30:30'),
(57, 'IVORY COAST', 'model', 'fsdfsd', '117.193.79.2', '2020-01-22 10:30:45', '2020-01-22 10:30:45'),
(58, 'KENYA', 'model', 'fsdfsd', '117.193.79.2', '2020-01-22 10:30:45', '2020-01-22 10:30:45'),
(59, 'IVORY COAST', 'model', 'sdfsdfd', '117.193.79.2', '2020-01-22 10:30:45', '2020-01-22 10:30:45'),
(60, 'KENYA', 'model', 'qwertyghj', '::1', '2020-08-31 23:41:43', '2020-08-31 23:41:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `is_admin`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'thasni', 'thasni@gmail.com', '0', NULL, '$2y$10$Bn6ZbHiRXAuW.x0ox01ZNuoerr.GRcAVgu/wzzoxVAMhsHqFRJb2u', NULL, '2019-11-04 23:49:26', '2019-11-04 23:49:26'),
(2, 'admin', 'admin@sharpafrica.com', '1', NULL, '$2y$10$o1EcpB7zkelXjnkQWqDl2usPU9ss7xgqDKUnp68yyVYDSqHsD7wB.', NULL, '2019-11-07 06:28:20', '2019-11-07 06:28:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unverified_products`
--
ALTER TABLE `unverified_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `unverified_products`
--
ALTER TABLE `unverified_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
