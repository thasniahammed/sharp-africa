
@extends('layouts.frontend.front_design')
@section('content')
     <div class="container">
        <div class="row">
            <div class="col-lg-12 text-left"><h1 class="error"><b>SHARP MFP / Copier - Verify Genuine Product</b></h1></div>
         </div>
    </div>
    <div class="red"></div>
    <div class="container" >
        <div class="row">
            
            
            <div class="col-lg-12" style="background-color: red;">
            
            <h3> <b>Result Multiple Product Serials</b></h3>
              <div class="row">
                    <div class="col-lg-1 col-md-1 text-left hidden-xs hidden-sm">
                       <label for="email" >Item </label>
                    </div>
                    <div class="col-lg-5 col-md-5 text-left hidden-xs hidden-sm">
                       <label>Product Serial Number </label>
                    </div>
                    <div class="col-lg-5 col-md-5 text-left hidden-xs hidden-sm">
                       <label for="email" >Country/Region of purchase </label>
                    </div>
                    <div class="col-lg-5 col-md-5 text-left hidden-xs hidden-sm">
                        <label for="email" >Model </label>
                     </div>
                    <div class="col-lg-1 col-md-1 text-center hidden-xs hidden-sm" >
                       <label for="email" >Reset </label>
                    </div>
                    </div>
               
@if(count($tempArr)>0)
 <!--second search start-->

 @foreach($tempArr as $key=>$value)
 <div class="row sharath">
    <div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-1 text-left ">
        <div class="numbers"><label>{{$value['item']+1}}</label></div>
    </div>
   
    <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-9 text-left">
<label for="serial" class="hidden-lg hidden-md">Serial Number <span class="needed">*</span> <span class="how"><a href="#" data-toggle="modal" data-target="#myModal" id="myBtn"><i class="fas fa-question-circle"></i></a></span></label>
<span>{{$value['sn']}}</span>
       
</div>
        
       <div class="clearfix hidden-lg hidden-md hidden-sm"></div>
        <div class="form-group hidden-lg hidden-md hidden-sm col-xs-1 text-center">
        </div>
        
        <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-9 ">
<label for="email" class="hidden-lg hidden-md">Country <span class="needed">*</span></label>
<span>{{$value['country']}}</span>

</div>
<div class="clearfix hidden-lg hidden-md hidden-sm"></div>
        <div class="form-group hidden-lg hidden-md hidden-sm col-xs-1 text-center">
        </div>

        <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-9 ">
            <label for="email" class="hidden-lg hidden-md">Model <span class="needed">*</span></label>
            <span>{{$value['model']}}</span>
            
            </div>      
<div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center">
  <div class="reset">
  <a href="#"><i class="far fa-times-circle fa-3x"></i></a>
   </div>
  </div>
       
    </div>
    @endforeach
    <!--second search ends-->

    @endif             

                
                
               
            
            </div>
    
   
    <div class="row">
        <div class="col-lg-1 col-md-1 text-left hidden-xs hidden-sm">
           <label for="email" >Item <span class="needed"></span></label>
        </div>
        <div class="col-lg-5 col-md-5 text-left hidden-xs hidden-sm">
           <label>Product Serial Number  </label>
        </div>
        <div class="col-lg-5 col-md-5 text-left hidden-xs hidden-sm">
           <label for="email" >Country/Region of purchase </label>
        </div>
        <div class="col-lg-5 col-md-5 text-left hidden-xs hidden-sm">
            <label for="email" >ModelNo </label>
         </div>
        <div class="col-lg-1 col-md-1 text-center hidden-xs hidden-sm" >
           <label for="email" >Reset <span class="needed"></span></label>
        </div>
        </div>
    @if(count($tempArr)>0)
 <!--second search start-->

 @foreach($tempArr as $key=>$value)
 <div class="row sharath">
    <div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-1 text-left ">
        <div class="numbers"><label>{{$value['item']}}</label></div>
    </div>
   
    <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-9 text-left">
<label for="serial" class="hidden-lg hidden-md">Serial Number <span class="needed">*</span> <span class="how"><a href="#" data-toggle="modal" data-target="#myModal" id="myBtn"><i class="fas fa-question-circle"></i></a></span></label>
<span>{{$value['sn']}}</span>
       
</div>
        
       <div class="clearfix hidden-lg hidden-md hidden-sm"></div>
        <div class="form-group hidden-lg hidden-md hidden-sm col-xs-1 text-center">
        </div>
        
        <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-9 ">
<label for="email" class="hidden-lg hidden-md">Country <span class="needed">*</span></label>
<span>{{$value['country']}}</span>

</div>
<div class="clearfix hidden-lg hidden-md hidden-sm"></div>
        <div class="form-group hidden-lg hidden-md hidden-sm col-xs-1 text-center">
        </div>

        <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-9 ">
            <label for="email" class="hidden-lg hidden-md">Model <span class="needed">*</span></label>
            <span>{{$value['model']}}</span>
            
            </div>      
<div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center">
  <div class="reset">
  <a href="#"><i class="far fa-times-circle fa-3x"></i></a>
   </div>
  </div>
       
    </div>
    @endforeach
    <!--second search ends-->

    @endif
    </div>
        </div>
     <div class="container">
    <div class="row">
   <hr>
         </div>  
         </div> 
        <div class="container">
    <div class="row">
      <div class="col-lg-12">
                <h3><b>Verify single product serial</b></h3>
         </div>
         <div class="col-lg-10">
                
        <p class="multiple">Have only one product to check? Use our single serial check option</p>
        </div>
            <div class="col-lg-2 text-right"><a href="{{url('/')}}" class="btn btn-danger btn-block btn-lg" >Go Back</a> </div>
            </div>
    </div>
    <div class="container">
    <div class="row">
   <hr>
         </div>  
         </div> 
    <div class="container">
    <div class="row">
      
         <div class="col-lg-12">
                
        <p class="multiple">For further support, please contact : <br>
<i class="wapp fab fa-whatsapp"></i> +971 56 501 5608 &nbsp;&nbsp;&nbsp;(WhatsApp only) <br>
<span>

<i class="far fa-envelope email"></i> <a href="mailto:info@sharpafrica.ae">info@sharpafrica.ae </a></span>(Email)</p>
        </div>
            
            </div>
    </div>
    <div class="container">
    <div class="row">
   <hr>
         </div>  
         </div> 
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">How do I find my product serial number?</h4>
      </div>
      <div class="modal-body">
       <div class="col-lg-5"><img class="slnumber" src="{{url('assets/frontend/images/Product_number_serial_number.jpg')}}" alt=""/></div>
               <div class="col-lg-7">
          <p>Select an appropriate method for your product:</p>
<h3>Desktops & All-in-Ones</h3>

<p>Some products utilize the keyboard shortcut ‘Ctrl + Alt + S’; the product information will appear after a short delay.<br><br>

If that doesn’t work, this information can also be found on your bar code stickers, typically located on the back of your product.</p>
<h3>Notebooks</h3>

<p>Some products utilize the keyboard shortcut ‘Fn + Esc’; the product information will appear after a short delay.<br><br>

If that doesn’t work, this information can also be found on your bar code stickers, which may be located behind the battery.</p>

          </div>
          
          <div class="clearfix"></div>
      </div>
     
    </div>

  </div>
</div>

@endsection   