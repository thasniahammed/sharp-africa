<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SharpAfrica -admin</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

 
    <link rel="stylesheet" href="{{ url('/public/css/backend_css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('/public/css/backend_css/font-awesome.min.css') }}">

    <link rel="stylesheet" href="{{ url('/public/css/backend_css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ url('/public/css/backend_css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ url('/public/css/backend_css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ url('/public/css/backend_css/scss/style.css') }}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>


</head>
<body>
@include('layouts.adminLayout.admin_sidebar')
@include('layouts.adminLayout.admin_header')

@yield('content')
 

    <script src="{{ url('/public/js/backend_js/jquery.min.js') }}"></script>
    <script src="{{ url('/public/js/backend_js/popper.min.js') }}"></script>
    <script src="{{ url('/public/js/backend_js/bootstrap.min.js') }}"></script>
    <script src="{{ url('/public/js/backend_js/main.js') }}"></script>


   
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>






 <script>
  tinymce.init({
    mode : "textareas",
    plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true

  });
</script

</body>
</html>
