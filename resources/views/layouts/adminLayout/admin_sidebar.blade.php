 <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
               
                <a  href="{{url('/admin/dashboard')}}"><img src="{{ url('public/assets/frontend/images/logo.png')}}" width="130" height="50" style="margin-top: 7px;" alt=""/></a>
                <a class="navbar-brand" href="{{url('/admin/dashboard')}}">Sharpafrica</a>
                <a class="navbar-brand hidden" href="{{url('/admin/dashboard')}}">SharpAfrica</a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="{{url('/admin/dashboard')}}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>

                    <!-- <li class="menu-item dropdown">
                        <a href="{{url('/admin/users')}}"> <i class="menu-icon fa fa-user"></i>Users </a>
                    </li> -->
                    <li class="menu-item dropdown">
                        <a href="{{url('/admin/products')}}"> <i class="menu-icon fa fa-product-hunt"></i>Products </a>
                    </li>
                    <li class="menu-item dropdown">
                        <a href="{{url('/admin/un-verify-products')}}"> <i class="menu-icon fa fa-shield"></i>Unverified Products </a>
                    </li>
                      <!-- <h3 class="menu-title">Banner </h3> --><!-- /.menu-title -->

                    
                   <!--  <h3 class="menu-title">Durations </h3> -->

                   <!--  <h3 class="menu-title">Inclusions </h3> -->

                   
                   <!--  <h3 class="menu-title">Package Details </h3> -->
                    <li class="menu-item dropdown">
                        <a href="{{url('/admin/dataimport')}}"> <i class="menu-icon fa fa-user"></i>Upload Excel </a>
                    </li>
                   
                    
                    
                   
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->