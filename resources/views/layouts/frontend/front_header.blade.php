<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Altech Digi Prints</title>
    <!-- Meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <meta name="keywords" content=""
    />
    <script>
        addEventListener("load", function () { setTimeout(hideURLbar, 0); }, false); function hideURLbar() { window.scrollTo(0, 1); }
    </script>
    <!-- Meta tags -->
    <!-- font-awesome icons -->
    <!--<link href="css/font-awesome.min.css" rel="stylesheet">-->
    <!-- //font-awesome icons -->
    <!--stylesheets-->
    
    <link href="{{ url('/public/assets/frontend/css/style.css')}}" rel='stylesheet' type='text/css' media="all">
      <link href="{{ url('/public/assets/frontend/css/bootstrap.css')}}" rel='stylesheet' type='text/css' media="all">
     <link href="{{ url('/public/assets/frontend/css/jquery-customselect-1.9.1.css')}}" rel='stylesheet' type='text/css' media="all">
    <script src="https://kit.fontawesome.com/fbe2011133.js" crossorigin="anonymous"></script>
    <!--//style sheet end here-->
   <!-- <link href="https://fonts.googleapis.com/css?family=Big+Shoulders+Text:400,700&display=swap" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Big+Shoulders+Display:400,500|Open+Sans+Condensed:300&display=swap" rel="stylesheet"> -->
    <script type="text/javascript" src="{{ url('/public/assets/frontend/js/jquery-3.4.1.min.js')}}"></script>
    <script type="text/javascript" src="{{ url('/public/assets/frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{ url('/public/assets/frontend/js/jquery-customselect.js')}}"></script>
</head>

<body>
    <div class="tophead">
    <div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><img class="img-responsive float-lt" src="{{ url('/public/assets/frontend/images/altechlogo.jpg')}}" alt=""/></div>
        <!--<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center"><div class="email">www.sharpafrica.ae</div></div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center"><div class="wapp"><i class="fab fa-whatsapp"></i> +9715572345</div></div>-->
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><img class="img-responsive float-rt" src="{{ url('/public/assets/frontend/images/sharp.jpg')}}" alt=""/></div>
      </div>
    </div>
        </div>