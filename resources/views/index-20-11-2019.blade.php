@extends('layouts.frontend.front_design')

@section('content')
<div class="container">
        <div class="row">
            <div class="col-lg-12 text-left"><h1 class="error"><b>SHARP MFP / Copier - Verify Genuine Product</b></h1></div>
         </div>
    </div>
    <div class="red"></div>
    <div class="container">
        <div class="row">
            
            
            <div class="col-lg-5">
            
            <h3><b>Verify Single Product Serial</b></h3>
                <div class="txt-left-side text-left">
                
                <!--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget</p>-->
                <form action="#" method="post" id="frmsharp">
                {{ csrf_field() }}
      <div class="form-group text-left">
      <p><span class="needed">*</span> <i>Required Fields</i></p>
      <!--loader-->
      <p>
        <label id="lbl-loader" style="display: none;">
        <img src="{{url('/public/assets/frontend/images/loader.gif')}}" id="Loading">
        </label>
    </p>
    <!--loader-->
    <label for="email">Country/Region of purchase <span class="needed">*</span></label>
   <select id='country' name='country' class='custom-select' required="true">
   <option value=''>Select Country</option>
     @foreach($countries as $key=>$value)
     <option value="{{$value->COUNTRIES}}">{{$value->COUNTRIES}}</option>

     @endforeach
      
    </select>
  </div>
    <div class="form-group text-left">
      <label for="serial">Product Serial Number <span class="needed">*</span> <span class="how"><a href="#" data-toggle="modal" data-target="#myModal" id="myBtn"><i class="fas fa-question-circle"></i></a></span></label>
      <input type="text" class="form-control" id="serial_no" name="serial_no" required="true" >
      <p style="font-size: 16px; margin-top:5px;">Example: 8307653X</p>
    </div>

     <p>I consent that the data submitted will be used in accordance with Altech's Privacy Policy</p>
      <div class="btnn text-right">
          <button id="search" class="btn btn-danger btn-block btn-lg"  type="submit"
          style="background-color: #d9534f;width: 159px;">Check Genuine</button>
      </div>
      <script>
      $(function() {
        $("#standard").customselect();
      });
      </script>
  </form>
                

            </div>
            </div>
    
    <div class="w3layouts-two-grids col-lg-7">
       
        <div class="mid-class" id="resultdiv">
            
            
            
        </div>
    </div>
    </div>
        </div>
     <div class="container">
    <div class="row">
   <hr>
         </div>  
         </div> 
        <div class="container">
    <div class="row">
      <div class="col-lg-12">
                <h3><b>Verify Multiple Product Serials</b></h3>
         </div>
         <div class="col-lg-10">
                
        <p class="multiple">Have more than one product to check? Use our multiple serial check option</p>
        </div>
            <div class="col-lg-2 text-right">
              <a href="{{url('/multiple-products')}}" class="btn btn-danger btn-block btn-lg" >Continue</a> </div>
            </div>
    </div>
    <div class="container">
    <div class="row">
   <hr>
         </div>  
         </div> 
    <div class="container">
    <div class="row">
      
         <div class="col-lg-12">
          <p class="multiple">For further support, please contact : <br>
          <i class="wapp fab fa-whatsapp"></i> +971 56 501 5608 &nbsp;&nbsp;&nbsp;(WhatsApp only) <br>
          <span>
          <i class="far fa-envelope email"></i> 
          <a href="mailto:info@sharpafrica.ae">info@sharpafrica.ae </a></span>(Email)</p>
        </div>
            
            </div>
    </div>
    <div class="container">
    <div class="row">
   <hr>
         </div>  
         </div> 
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">How do I find my product serial number?</h4>
      </div>
      <div class="modal-body">
       <div class="col-lg-5"><img class="slnumber" src="{{ url('/public/assets/frontend/images/Product_number_serial_number.jpg')}}" alt=""/></div>
               <div class="col-lg-7">
          <p>Select an appropriate method for your product:</p>
<h3>Desktops & All-in-Ones</h3>

<p>Some products utilize the keyboard shortcut ‘Ctrl + Alt + S’; the product information will appear after a short delay.<br><br>

If that doesn’t work, this information can also be found on your bar code stickers, typically located on the back of your product.</p>
<h3>Notebooks</h3>

<p>Some products utilize the keyboard shortcut ‘Fn + Esc’; the product information will appear after a short delay.<br><br>

If that doesn’t work, this information can also be found on your bar code stickers, which may be located behind the battery.</p>

          </div>
          
          <div class="clearfix"></div>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>-->
    </div>

  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">

  $("#Loading").fadeOut('fast');

  $.ajaxSetup({

      headers: {

          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

      }

  });



  $('#frmsharp').on('submit', function(e){

      e.preventDefault();
   var formData = new FormData($(this)[0]);
   console.log(formData);
  $("#Loading").fadeIn();
     $.ajax({

         type:'POST',

         url:'{{ url("/frmsharpsubmit") }}',

         data:new FormData(this),
         dataType:'JSON',
          contentType: false,
          cache: false,
          processData: false,
         success:function(data){

            $("#Loading").fadeIn();
           console.log(data.data);
            if(data.data!=null){
              $('#resultdiv').html('<div class="img-right-side" style="margin-top:110px;"><div class=""><div class="col-lg-12 text-center"><img src="{{url("/public/assets/frontend/images/right.png")}}" alt="" width="100" height="100"/></div><br><div class="col-lg-12"><p>Congratulations... Your product has been verified as Genuine!</p><h4>Model No: '+data.data['model']+'<br><span class="serial">Serial No: '+data.data['sn']+'</span></h4></div></div></div>');
            }else{
              $('#resultdiv').html('<div class="img-right-side" style="margin-top:110px;"><div class=""><div class="col-lg-12 text-center"><img src="{{url("/public/assets/frontend/images/wrong.png")}}" alt="" width="100" height="100"/></div><br><div class="col-lg-12"><p>Sorry...Couldnt find your product!</p></div></div></div>');
            }
            
         
         }

      });

$("#Loading").fadeOut('fast');

});

</script>
@endsection
