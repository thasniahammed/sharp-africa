 @extends('layouts.adminLayout.admin_design')
  @section('content')

   <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                       <h1><a href="{{url('/admin/dashboard')}}" class="btn btn-success">Dashboard </a>
                        <a href="{{url('/admin/users')}}" class="btn btn-success">Users</a></h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active">Users</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                  <div class="col-lg-10">
                    <div class="card">
                      <div class="card-header">
                        <strong>Edit</strong> User
                      </div>

                      <div class="card-body card-block">
                        <form action="{{url('/admin/edit-user-submit/'.$user->id)}}" method="post"  class="form-horizontal">
                          {{ csrf_field() }}
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">User Name</label></div>
                            <div class="col-12 col-md-9"><input type="text" name="name" required="true" id="text-input" value="{{$user->name}}"  placeholder="User  Name" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Email  </label></div>
                            <div class="col-12 col-md-9"><input type="text" name="email" 
                              value="{{$user->email}}" required="true" readonly="true"  id="text-input"   class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Phone No</label></div>
                            <div class="col-12 col-md-9"><input type="text" name="phone_no" value="{{$user->phone_no}}" required="true"  id="text-input"  class="form-control"></div>
                          </div>
                          
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Address</label></div>
                            <div class="col-12 col-md-9">
                              <input type="text" required="true" name="address" class="form-control"  id="address" 
                              style="width: 612px;height: 50px;" 
                              value="{{$user->address}}">
                             
                             </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Gender</label></div>
                            <div class="col-12 col-md-9">
                              <select class="required form-control" required="true" name="gender" 
                    id="gender">
                      <option value="">Gender</option>
                      <option value="male" {{ $user->gender == "male" ? 'selected="selected"' : '' }}>Male</option>
                      <option value="female" {{ $user->gender == "female" ? 'selected="selected"' : '' }}>Female</option>
                       
                    </select>
                              <!-- <input type="text" name="gender" value="{{$user->gender}}"  id="text-input"  class="form-control"> --></div>
                          </div>
                         <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Date of Birth</label></div>
                            <div class="col-12 col-md-9"><input type="date" name="dateofbirth" value="{{$user->dob}}"   id="text-input"  class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Marital Status</label></div>
                            <div class="col-12 col-md-9">
                               <select class="required form-control" name="marital_status" 
                    id="marital_status" required="true">
                      <option value="">Marital Status</option>
                      <option value="1" {{ $user->marital_status == 1 ? 'selected="selected"' : '' }}>Married</option>
                      <option value="2" {{ $user->marital_status == 2 ? 'selected="selected"' : '' }}>Unmarried</option>
                       
                    </select>
                              <!-- @if($user->marital_status=='1')
                              <input type="text" name="marital_status" value="Married" id="text-input"  class="form-control">
                              @else
                              <input type="text" name="marital_status" value="Unmarried" id="text-input"  class="form-control">
                              @endif -->
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">State</label></div>
                            <div class="col-12 col-md-9"><input type="text" name="state" value="{{$user->state}}" id="text-input" required="true"  class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Zipcode</label></div>
                            <div class="col-12 col-md-9"><input type="text" name="zipcode" value="{{$user->zipcode}}" required="true" id="text-input"  class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">City</label></div>
                            <div class="col-12 col-md-9"><input type="text" name="city" value="{{$user->city}}" id="text-input" required="true"  class="form-control"></div>
                          </div>
                          <div class="row ">
                            <div class="col col-md-3"></div>
                            <div class="col-12 col-md-9"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label"></label></div>
                            <div class="col-12 col-md-9"><button type="submit" class="btn btn-primary btn-sm">
                          <i class="fa fa-dot-circle-o"></i> Submit
                        </button></div>
                          </div>
                          </div>
                          
                         
                         
                       
                      
                    </div>
                    



                </div><!-- .row -->
            </div><!-- .animated -->
        </div><!-- .content -->
  </div><!-- /#right-panel -->

    <!-- Right Panel -->
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

<!--  <link href="{{url('public/css/backend_css/jquery-ui.css')}}" rel="stylesheet"> -->

<script type="text/javascript">
  $(function() {

  $('input[name="dateofbirth"]').datepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });
 
});
</script>
  @endsection