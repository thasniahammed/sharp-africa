{{-- \resources\views\users\create.blade.php --}}
@extends('layouts.app')
@section('title', '| Create User')
@section('content')

<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
      <div class="page-title">
        <h1>Create User</h1>
      </div>
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li>
            <a href="#">Dashboard</a>
          </li>

          <li class="active">Create User</li>
        </ol>
      </div>
    </div>
  </div>
</div>


<div class="content mt-3">
  <div class="animated fadeIn">
    <div class="row">

      <div class="col-lg-12">
        <div class="card">
          <div class="card-header y-color">
            <strong class="card-title">Create User</strong>

          </div>
          <div class="card-body">
 @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 

<div class='col-lg-4 col-lg-offset-4'>
   
    {!! Form::open(array('url' => 'users')) !!}
    <div class="form-group @if ($errors->has('name')) has-error @endif">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', '', array('class' => 'form-control')) !!}
    </div>
      <div class="form-group @if ($errors->has('name_of_startup')) has-error @endif">
        {!! Form::label('name_of_startup', 'Name of startup') !!}
        {!! Form::text('name_of_startup', '', array('class' => 'form-control')) !!}
    </div>
<div class="form-group @if ($errors->has('project_name')) has-error @endif">
        {!! Form::label('project_name', 'Name of Project') !!}
        {!! Form::text('project_name', '', array('class' => 'form-control')) !!}
    </div>
  <div class="form-group @if ($errors->has('mobile')) has-error @endif">
        {{ Form::label('mobile', 'Mobile') }}
        {{ Form::text('mobile', '', array('class' => 'form-control')) }}
    </div>
    <div class="form-group @if ($errors->has('email')) has-error @endif">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email', '', array('class' => 'form-control')) !!}
    </div>
  
<!--    
    <div class="form-group @if ($errors->has('password')) has-error @endif">
        {!! Form::label('password', 'Password') !!}<br>
        {!! Form::password('password', array('class' => 'form-control')) !!}
    </div>
    <div class="form-group @if ($errors->has('password')) has-error @endif">
        {!! Form::label('password', 'Confirm Password') !!}<br>
        {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
    </div> -->


 {!! Form::hidden('roles[]', 3,'', array('class' => 'role') ) !!}
           

 <div class="form-group @if ($errors->has('status')) has-error @endif">
        {!! Form::label('Status', 'Status') !!}<br>
   
{{ Form::radio('status', '1', old('status'), ['class' => 'status']) }}Active
{{ Form::radio('status', '0', old('status'), ['class' => 'status']) }}Inactive


    </div>



 <div class="form-group @if ($errors->has('usertype')) has-error @endif">
        {!! Form::label('User type', 'User type') !!}<br>
      
{{ Form::radio('usertype', 'startup', old('usertype'), ['class' => 'usertype']) }}Startup
{{ Form::radio('usertype', 'student', old('usertype'), ['class' => 'usertype']) }}Student


    </div>



<div class="form-group @if ($errors->has('fund')) has-error @endif" >

{!! Form::label('Fund type', 'Fund type') !!}<br>
      
{!! Form::radio('fund', '2',  (old('fund') ==  '2'), array( 'class'=>'type','id'=>'grant')) !!}Grant

{!! Form::radio('fund', '1',  (old('fund') ==  '1'), array( 'class'=>'type','id'=>'seed')) !!}Seed
</div>

    <div class="form-group   ideaamount @if ($errors->has('amount')) has-error @endif">
        {!! Form::label('amount', 'Grant Amount') !!}

    
        {!! Form::number('ideaamount', old('ideaamount'), array('class' => 'form-control ideaamount')) !!}
       

    </div>

     <div class="form-group  ideano @if ($errors->has('ideano')) has-error @endif">


    {!! Form::label('amount', 'Grant No') !!}
       
  

{!! Form::select('ideano', array( '' => 'Choose Grant no', 'Idea Day 1' => 'Idea Day 1', 'Idea Day 2' => 'Idea Day 2', 'Idea Day 3' => 'Idea Day 3', 'Idea Day 4' => 'Idea Day 4' ,'Idea Day 5' => 'Idea Day 5', 'Idea Day 6' => 'Idea Day 6', 'Idea Day 7' => 'Idea Day 7','Idea Day 8' => 'Idea Day 8', 'Idea Day 9' => 'Idea Day 9', 'Idea Day 10' => 'Idea Day 10', 'Idea Day 11' => 'Idea Day 11', 'Idea Day 12' => 'Idea Day 12', 'Idea Day 13' => 'Idea Day 13', 'Idea Day 14' => 'Idea Day 14', 'Idea Day 15' => 'Idea Day 15','Idea Day 16' => 'Idea Day 16', 'Idea Day 17' => 'Idea Day 17', 'Idea Day 18' => 'Idea Day 18', 'Idea Day 19' => 'Idea Day 19', 'Idea Day 20' => 'Idea Day 20'), old('ideano'), array('class' => 'form-control ideano')) !!}


    </div>

    <div class="form-group   trancheamount @if ($errors->has('trancheamount')) has-error @endif">
        {!! Form::label('trancheamount', 'First Tranche Amount') !!}

    
        {!! Form::number('trancheamount', old('trancheamount'), array('class' => 'form-control trancheamount')) !!}
       

    </div>


    <div class="form-group amountseed @if($errors->has('amount')) has-error @endif">
        {!! Form::label('amount', 'Seed Amount') !!}
       
        {!! Form::number('amountseed', old('amountseed'), array('class' => 'form-control amountseed')) !!}
      
    </div>

    
    <div class="form-group @if ($errors->has('user_sector')) has-error @endif">
        {!! Form::label('user_sector', 'Sectors') !!}
        {!! Form::text('user_sector', '', array('class' => 'form-control')) !!}
    </div>
    <div class="form-group @if ($errors->has('user_category')) has-error @endif">
        {!! Form::label('user_category', 'Category') !!}
        {!! Form::text('user_category', '', array('class' => 'form-control')) !!}
    </div>
    
    {!! Form::submit('Register', array('class' => 'btn btn-primary')) !!}
    {!! Form::close() !!}
</div>
@endsection