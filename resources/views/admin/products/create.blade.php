@extends('layouts.adminLayout.admin_design')
  @section('content')
 
</style>
  <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active">Prouducts</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                  <div class="col-lg-10">
                    <div class="card">
                      <div class="card-header">
                        <strong>Add</strong> Products
                      </div>

                      <div class="card-body card-block">
                        <form action="{{url('/admin/add-product')}}" method="post"  class="form-horizontal">
                          {{ csrf_field() }}
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Serial No</label></div>
                            <div class="col-12 col-md-9"><input type="text" name="sn" required="true" id="text-input" value=""  placeholder="Serail No" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Model  </label></div>
                            <div class="col-12 col-md-9"><input type="text" name="model" 
                              value="" required="true"   id="text-input"   class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Country</label></div>
                            <div class="col-12 col-md-9">
                            <select id='country' name='country' class='custom-select' required="true">
                            <option value=''>Select Country</option>
                              @foreach($countries as $key=>$value)
                              <option value="{{$value->COUNTRIES}}" >{{$value->COUNTRIES}}</option>

                              @endforeach
                                
                              </select>
                            </div>
                          </div>
                          
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Customer</label></div>
                            <div class="col-12 col-md-9">
                              <input type="text" required="true" name="customer" class="form-control"  id="customer" 
                               value="">
                             
                             </div>
                          </div>
                         
                         <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Date</label></div>
                            <div class="col-12 col-md-9"><input type="date" name="date" value=""   id="text-input"  class="form-control"></div>
                          </div>
                          
                          <div class="row ">
                            <div class="col col-md-3"></div>
                            <div class="col-12 col-md-9"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label"></label></div>
                            <div class="col-12 col-md-9">
                              <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                          </div>
                          </div>
                        </form>

                    </div>
                    



                </div><!-- .row -->
            </div><!-- .animated -->
        </div><!-- .content -->
  </div><!-- /#right-panel -->

    <!-- Right Panel -->
    @endsection