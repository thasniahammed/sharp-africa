@extends('layouts.adminLayout.admin_design')
  @section('content')
 
</style>
  <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1><a href="{{url('/admin/products')}}" class="btn btn-success">Products </a></h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active"><a href="{{url('/admin/products')}}">Prouduct List</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                  <div class="col-lg-10">
                    <div class="card">
                      <div class="card-header">
                        <strong>Add</strong> Products
                        <label style="float:right;"><a href="{{url('/public/download/sample.xlsx')}}" class="btn btn-success">Sample Excel download</a></label>
                      </div>
                      @if(Session::has('flash_message_success'))

<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{!! session('flash_message_success') !!}</strong>
</div>
@endif
                      <div class="card-body card-block">
                     
                        <form  action="{{ url('/admin/importdata-post') }}" method="post" 
                        enctype="multipart/form-data"  name="frmuserimport">
                        {{ csrf_field() }}
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">  Upload Excel</label></div>
                            <div class="col-12 col-md-6"> <input class="inp" type="file"  name="select_file" id="select_file" /></div>
                            <input type="submit" class="btn btn-primary btn-sm" value="Go">
                        </div>
                        
                         
<!--                           
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Customer</label></div>
                            <div class="col-12 col-md-9">
                              <input type="text" required="true" name="customer" class="form-control"  id="customer" 
                               value="">
                             
                             </div>
                          </div>
                         
                         <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Date</label></div>
                            <div class="col-12 col-md-9"><input type="date" name="date" value=""   id="text-input"  class="form-control"></div>
                          </div> -->
                          
                          <!-- <div class="row ">
                            <div class="col col-md-3"></div>
                            <div class="col-12 col-md-9"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label"></label></div>
                            <div class="col-12 col-md-9">
                              <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                          </div>
                          </div> -->
                        </form>

                    </div>
                    



                </div><!-- .row -->
            </div><!-- .animated -->
        </div><!-- .content -->
  </div><!-- /#right-panel -->

    <!-- Right Panel -->
    @endsection