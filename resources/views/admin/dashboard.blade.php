@extends('layouts.adminLayout.admin_design')
@section('content')


<!-- Right Panel -->

  

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1></h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                        <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                            <li class="active"></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-1" style="background: linear-gradient(to right,#fe9365,#feb798);text-align: center;">
                    <div class="card-body pb-0">
                        <div class="dropdown float-right">
                        
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <div class="dropdown-menu-content">
                                    <a class="dropdown-item" href="#">Action</a>
                                                  </div>
                            </div>
                        </div>
                        <p class="text-light">
                            <a href="{{url('/admin/products')}}"><b>PRODUCTS</b></a></p>
                        <h4 class="pb-20">
                            <span><P>&nbsp;</P></span>
                        </h4>
                       

                    </div>

                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-1" style="background: linear-gradient(to right,#0ac282,#0df3a3);text-align: center;">
                    <div class="card-body pb-0">
                        <div class="dropdown float-right">
                        
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <div class="dropdown-menu-content">
                                    <a class="dropdown-item" href="#">Action</a>
                                                  </div>
                            </div>
                        </div>
                        <p class="text-light">
                            <a href="{{url('/admin/un-verify-products')}}"><b>UNVERIFIED PRODUCTS</b></a></p>
                        <h4 class="pb-20">
                            <span><P>&nbsp;</P></span>
                        </h4>
                       

                    </div>

                </div>
            </div>

            
                


                </div><!-- .row -->
            </div><!-- .animated -->
        </div><!-- .content -->
  </div><!-- /#right-panel -->
    <!-- Right Panel -->
    @endsection