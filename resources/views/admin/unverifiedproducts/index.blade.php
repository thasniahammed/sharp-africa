 @extends('layouts.adminLayout.admin_design')
 @section('content')
 <meta name="csrf-token" content="{!! csrf_token() !!}">
 <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1><a href="{{url('/admin/dashboard')}}" class="btn btn-success">Dashboard </a>
                        <!-- <a href="{{url('/admin/view-banner')}}" class="btn btn-success">Banners</a> --></h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Products</a></li>
                            <li class="active">List</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                      @if(Session::has('flash_message_error'))

                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{!! session('flash_message_error') !!}</strong>
                    </div>
                    @endif
                    @if(Session::has('message'))

                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ Session::get('message') }}</strong>
                    </div>
                    @endif
                    
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Products</strong>
                            <p  id="lblmsg"></p>
                        </div>
                        <div class="card-body">
                          
                    <a href="#" class="btn btn-warning add" id="deleteAll">Delete All</a>
                  <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                    <thead>
                        
                      <tr>
                       
                        <th>Sl.No</th>
                        <th>Serial No</th>
                        <th>Country </th>
                          <th>Ip address </th>
                          <th>Updated Time</th>
                        <th>Actions</th>
                        <th><input type="checkbox" id="select_all" /></th> 
                        

                      </tr>
                    </thead>
                    <tbody>
                        <?php
                            $i=1;
                        ?>
                        @foreach($products as $product)
                      <tr>
                        
                        <td>{{$i++}}</td>
                        <td>{{$product->serial_no}}</td>
                       <td>{{ $product->country }}</td>
                        <td>{{ $product->ip_address }}</td>
                        <td>{{ $product->updated_at }}</td>
                       
                       
                        <td>
                      
                        <a class="btn btn-danger btn-sm"  href="{{ url('/admin/delete-un-vproduct/'.$product->id) }}" onclick="return confirm('Do you want to delete?');">Delete</a> 
                        </td>
                        <td><input type="checkbox" class="checkbox" data-id="{{$product->id}}" value="{{$product->id}}"/></td>
                      </tr>
                      
                        @endforeach
                        
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->
    <script src="{{url('/public/js/backend_js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/pdfmake.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/datatables-init.js')}}"></script>



   

<script type="text/javascript">
$(document).ready(function(){
    $('#select_all').on('click',function(){

        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
            });
        }

    });
    
    $('.checkbox').on('click',function(){

        if($('.checkbox:checked').length == $('.checkbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }

    });
});
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">

jQuery('#deleteAll').on('click', function(e) { 
var allVals = [];  
		$(".checkbox:checked").each(function() {  
			allVals.push($(this).attr('data-id'));
		});  
		//alert(allVals.length); return false;  
		if(allVals.length <=0)  
		{  
			alert("Please select row.");  
		}  
		else {  
			//$("#loading").show(); 
			WRN_PROFILE_DELETE = "Are you sure you want to delete this row?";  
			var check = confirm(WRN_PROFILE_DELETE);  
			if(check == true){  
				//for server side
				
				var join_selected_values = allVals.join(","); 
				
				$.ajax({   
                  
                   
					type: "POST",  
					url: "./deleteAll",  
                   
                    data: {"_token": $('meta[name="csrf-token"]').attr('content'),"id": join_selected_values},
					success: function(response)  
					{   
                      
                       var pageurl="{{url('/admin/un-verify-products')}}";
                      
                      
                        $("#lblmsg").html("Selected Product Deleted Successfully!!");
                        $('#lblmsg').attr('style', 'font-size:14px;color:green;');
                        window.location.href = pageurl;
					}   
				});
              
			 
				

			}  
		}  
	});
    </script>
    <!-- Right Panel -->
    @endsection