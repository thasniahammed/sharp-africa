 @extends('layouts.adminLayout.admin_design')
  @section('content')

   <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                       <h1><a href="{{url('/admin/dashboard')}}" class="btn btn-success">Dashboard </a>
                        <a href="{{url('/admin/products')}}" class="btn btn-success">Products</a></h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li class="active">Products</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                  <div class="col-lg-10">
                    <div class="card">
                      <div class="card-header">
                        <strong>Edit</strong> Product
                      </div>

                      <div class="card-body card-block">
                        <form action="{{url('/admin/edit-product-submit/'.$product->id)}}" method="post"  class="form-horizontal">
                          {{ csrf_field() }}
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Serial No</label></div>
                            <div class="col-12 col-md-9"><input type="text" name="sn" required="true" id="text-input" value="{{$product->sn}}"  placeholder="Serail No" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Model  </label></div>
                            <div class="col-12 col-md-9"><input type="text" name="model" 
                              value="{{$product->model}}" required="true"   id="text-input"   class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Country</label></div>
                            <div class="col-12 col-md-9">
                            <select id='country' name='country' class='custom-select' required="true">
                            <option value=''>Select Country</option>
                              @foreach($countries as $key=>$value)
                              <option value="{{$value->COUNTRIES}}" {{ $value->COUNTRIES == $product->country ? 'selected="selected"' : '' }}>{{$value->COUNTRIES}}</option>

                              @endforeach
                                
                              </select>
                            </div>
                          </div>
                          
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Customer</label></div>
                            <div class="col-12 col-md-9">
                              <input type="text" required="true" name="customer" class="form-control"  id="customer" 
                              style="width: 612px;height: 50px;" 
                              value="{{$product->customer}}">
                             
                             </div>
                          </div>
                         @php 
                       
                         $date_field         = date('Y-m-d',strtotime($product->date));

                         @endphp
                         <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Date</label></div>
                            <div class="col-12 col-md-9"><input type="date" name="date" value="{{$date_field }}"   id="text-input"  class="form-control"></div>
                          </div>
                          
                          
                        
                          
                          <div class="row ">
                            <div class="col col-md-3"></div>
                            <div class="col-12 col-md-9"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label"></label></div>
                            <div class="col-12 col-md-9"><button type="submit" class="btn btn-primary btn-sm">
                          <i class="fa fa-dot-circle-o"></i> Submit
                        </button></div>
                          </div>
                          </form>
                          </div>
                          
                         
                         
                       
                      
                    </div>
                    



                </div><!-- .row -->
            </div><!-- .animated -->
        </div><!-- .content -->
  </div><!-- /#right-panel -->

    <!-- Right Panel -->
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

<!--  <link href="{{url('public/css/backend_css/jquery-ui.css')}}" rel="stylesheet"> -->

<script type="text/javascript">
  $(function() {

  $('input[name="dateofbirth"]').datepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });
 
});
</script>
  @endsection