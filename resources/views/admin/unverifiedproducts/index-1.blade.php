 @extends('layouts.adminLayout.admin_design')
 @section('content')
 <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1><a href="{{url('/admin/dashboard')}}" class="btn btn-success">Dashboard </a>
                        <!-- <a href="{{url('/admin/view-banner')}}" class="btn btn-success">Banners</a> --></h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Products</a></li>
                            <li class="active">List</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                      @if(Session::has('flash_message_error'))

                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{!! session('flash_message_error') !!}</strong>
                    </div>
                    @endif
                    @if(Session::has('message'))

                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ Session::get('message') }}</strong>
                    </div>
                    @endif
                    
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Products</strong>
                        </div>
                        <div class="card-body">
                    <!-- <a href="{{url('/admin/add-product')}}" class="btn btn-warning add ">Add Product</a> -->
                  <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Sl.No</th>
                        <th>Serial No</th>
                        
                          <th>Country </th>
                          <th>Ip address </th>
                          <th>Updated Time</th>
                        <th>Actions</th>
                        

                      </tr>
                    </thead>
                    <tbody>
                        <?php
                            $i=1;
                        ?>
                        @foreach($products as $product)
                      <tr>
                        <td>{{$i++}}</td>
                        <td>{{$product->sn}}</td>
                       
                        <td>{{ $product->country }}</td>
                        <td>{{ $product->ip_address }}</td>
                        <td>{{ $product->updated_at }}</td>
                       
                       
                        <td>
                      
                        <a class="btn btn-danger btn-sm"  href="{{ url('/admin/delete-un-vproduct/'.$product->id) }}" onclick="return confirm('Do you want to delete?');">Delete</a> 
                        </td>
                      </tr>
                      
                        @endforeach
                        
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->
    <script src="{{url('/public/js/backend_js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/pdfmake.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{url('/public/js/backend_js/lib/data-table/datatables-init.js')}}"></script>



    <script type="text/javascript">
        $(document).ready(function() {

            $('#bootstrap-data-table-export').DataTable({

                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]

            } );
        } );


    </script>



    <!-- Right Panel -->
    @endsection