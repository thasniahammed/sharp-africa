
@extends('layouts.frontend.front_design')
@section('content')
     <div class="container">
        <div class="row">
            <div class="col-lg-12 text-left"><h1 class="error"><b>SHARP MFP / Copier - Verify Genuine Product</b></h1></div>
         </div>
    </div>
    <div class="red"></div>
    <div class="container">
        <div class="row">
            
            
            <div class="col-lg-12">
            
            <h3> <b>Verify Multiple Product Serials</b></h3>
                <p>Manually enter up to twenty serial numbers below and choose ‘Check Genuine’.</p><br><br>
               
                <div class="row">
                    <div class="col-lg-1 col-md-1 text-left hidden-xs hidden-sm">
                       <label for="email" >Item <span class="needed"></span></label>
                    </div>
                    <div class="col-lg-5 col-md-5 text-left hidden-xs hidden-sm">
                       <label>Product Serial Number <span class="needed">*</span> <span class="how"><a href="#" data-toggle="modal" data-target="#myModal" id="myBtn"><i class="fas fa-question-circle"></i></a></span></label>
                    </div>
                    <div class="col-lg-5 col-md-5 text-left hidden-xs hidden-sm">
                       <label for="email" >Country/Region of purchase <span class="needed">*</span></label>
                    </div>
                    
                    <div class="col-lg-1 col-md-1 text-center hidden-xs hidden-sm" >
                       <label for="email" >Reset <span class="needed"></span></label>
                    </div>
                    </div>
               
                <form action="#" method="post" id="frmmultiple" name="frmmultiple">
                    {{ csrf_field() }}
                     <div class="bharath">
                     
                    @for ($i = 1; $i <=5; $i++)   
                    <div class="row sharath" id="showDiv_{{$i}}">
                    <div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-1 text-left ">
                        <div class="numbers"><label>{{$i}}</label></div>
                    </div>
                   
    <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-9 text-left">
    <label for="serial" class="hidden-lg hidden-md">Serial Number <span class="needed">*</span> <span class="how"><a href="#" data-toggle="modal" data-target="#myModal" id="myBtn"><i class="fas fa-question-circle"></i></a></span></label>
    <input type="text" class="form-control" id="serial_no" name="serial_no[]" 
    value="" required="true">
                       
  </div>
                        
  <div class="clearfix hidden-lg hidden-md hidden-sm"></div>
    <div class="form-group hidden-lg hidden-md hidden-sm col-xs-1 text-center">
      </div>
                        
    <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-9 ">
    <label for="email" class="hidden-lg hidden-md">Country <span class="needed">*</span></label>
    <select id='country' name='country[]' class='custom-select'  required="true" 
    >
        <option value=''>Select Country</option>
        @foreach($countries as $key=>$value)
        <option value="{{$value->COUNTRIES}}">{{$value->COUNTRIES}}</option>
   
        @endforeach
      
    </select>
  </div>
  <div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center">
            <div class="reset">
            <a href="#" id="delete_{{$i}}" name="{{$i}}" class="delebtn"><i class="far fa-times-circle fa-3x" aria-hidden="false"></i></a>
              </div>
  </div>
                       
                    </div>

                    @endfor  
                    <input type="hidden" name="total_count" id="total_count" value="{{$i-1}}"/>
                    <div id="addnewdiv" style="display: none;"> 
                      </div>
                    <!--First search-->
                   
                        
                       
                     
                         </div>
                         <a href="#!" id="addnew" class="btn btn-success btn-lg" style="float:right;">Add New</a>
                    <div class="clearfix"></div><br>
                    <p> <b>Please note:</b> Additional time may be required to look up multiple serial numbers. <br>
                    <br>
                    </p>
      <div class="row">
      <div class="col-lg-10">
      <p>I consent that the data submitted will be used in accordance with Altech's Privacy Policy</p></div>
          <div class="col-lg-2 text-right">
          <button id="search" name="search" class="btn btn-danger btn-lg"
          style="background-color: #d9534f;" type="submit">Check Genuine </button>
      </div>
      </div>
                    
                    <script>
      $(function() {
        $("#standard, #standard2, #standard3, #standard4, #standard5, #standard6, #standard7, #standard8, #standard9, #standard10, #standard11, #standard12, #standard13, #standard14, #standard15, #standard16, #standard17, #standard18, #standard19, #standard20").customselect();
      });
      </script>
                </form>
                
                
               
            
            </div>
    
    <div class="w3layouts-two-grids col-lg-12">
        
        <div class="mid-class" id="resultdivMain" style="display: none;">
            <table class="table">
                <thead id="thead">
                  <tr>
                    <th scope="col">Item No</th>
                    <th scope="col">Serial No</th>
                    <th scope="col">Country</th>
                    <th scope="col">Model</th>
                    <th scope="col">Status</th>
                  </tr>
                </thead>
                <tbody id="resultdiv">
                  
                 
                </tbody>
              </table>
            <!-- <div class="img-right-side">
                
                <div class="col-lg-12 text-center"><img src="{{url('/assets/frontend/images/right.png')}}" alt="" width="100" height="100"/></div><br>
        <div class="col-lg-12"> <p>Congratulations... Your product has been verified as Genuine!</p>
          <h4>Model No: NHS1234A <br>
<span class="serial">Serial No: 1234344668</span></h4>
                </div>
                
            </div> -->
        </div>
    </div>
   
   
    </div>
        </div>
     <div class="container">
    <div class="row">
   <hr>
         </div>  
         </div> 
        <div class="container">
    <div class="row">
      <div class="col-lg-12">
                <h3><b>Verify single product serial</b></h3>
         </div>
         <div class="col-lg-10">
                
        <p class="multiple">Have only one product to check? Use our single serial check option</p>
        </div>
            <div class="col-lg-2 text-right"><a href="{{url('/')}}" class="btn btn-danger btn-block btn-lg" >Go Back</a> </div>
            </div>
    </div>
    <div class="container">
    <div class="row">
   <hr>
         </div>  
         </div> 
    <div class="container">
    <div class="row">
      
         <div class="col-lg-12">
                
        <p class="multiple">For further support, please contact : <br>
<i class="wapp fab fa-whatsapp"></i> +971 56 501 5608 &nbsp;&nbsp;&nbsp;(WhatsApp only) <br>
<span>

<i class="far fa-envelope email"></i> <a href="mailto:info@sharpafrica.ae">info@sharpafrica.ae </a></span>(Email)</p>
        </div>
            
            </div>
    </div>
    <div class="container">
    <div class="row">
   <hr>
         </div>  
         </div> 
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">How do I find my product serial number?</h4>
      </div>
      <div class="modal-body">
       <div class="col-lg-5"><img class="slnumber" src="{{url('/public/assets/frontend/images/Product_number_serial_number.jpg')}}" alt=""/></div>
               <div class="col-lg-7">
          <p>Select an appropriate method for your product:</p>
<h3>Desktops & All-in-Ones</h3>

<p>Some products utilize the keyboard shortcut ‘Ctrl + Alt + S’; the product information will appear after a short delay.<br><br>

If that doesn’t work, this information can also be found on your bar code stickers, typically located on the back of your product.</p>
<h3>Notebooks</h3>

<p>Some products utilize the keyboard shortcut ‘Fn + Esc’; the product information will appear after a short delay.<br><br>

If that doesn’t work, this information can also be found on your bar code stickers, which may be located behind the battery.</p>

          </div>
          
          <div class="clearfix"></div>
      </div>
     
    </div>

  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">

  $("#Loading").fadeOut('fast');

  $.ajaxSetup({

      headers: {

          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

      }

  });



  $('#frmmultiple').on('submit', function(e){

      e.preventDefault();
      $("#resultdivMain").show();
      $('#resultdiv').html('');
   var formData = new FormData($(this)[0]);
   console.log(formData);
  $("#Loading").fadeIn();
     $.ajax({

         type:'POST',
         
         url:'{{ url("/multipleSubmit") }}',

         data:new FormData(this),
         dataType:'JSON',
          contentType: false,
          cache: false,
          processData: false,
         success:function(data){

            $("#Loading").fadeIn();
          
          
            console.log(data.dataArr);
            console.log(data.status);
            var count=1;

            if(data.status==1){
              
              $("#thead").hide();
              var status='<img src="{{url("/public/assets/frontend/images/right.png")}}" alt="" width="35" height="35"/>';
              $('#resultdiv').append('<tr><th scope="row"></th><td></td><td>Congratulations... Your products has been verified as Genuine! '+status+'</td><td></td><td></td></tr>');
           
            }else{

              $("#thead").show();
                $.each(data.dataArr, function(index, value) {
                
                $("#Loading").fadeIn();
                if(value['model']==null){
                  var status='<img src="{{url("/public/assets/frontend/images/wrong.png")}}" alt="" width="35" height="35"/>';
                }else{
                  var status='<img src="{{url("/public/assets/frontend/images/right.png")}}" alt="" width="35" height="35"/>';
                }
                if(value['serial_no']!=null)
                {
                  $('#resultdiv').append('<tr><th scope="row">'+count+'</th><td>'+value['serial_no']+'</td><td>'+value['country']+'</td><td>'+value['model']+'</td><td>'+status+'</td></tr>');
                }

               
                    count=count+1;


                }); 

            }
        
         
         }

      });

$("#Loading").fadeOut('fast');

});
//Remove div

$(document).on("click", ".delebtn", function(){

var name=$(this).attr('name');

 $('#showDiv_'+name).remove();

// count=count-1;
// $('#total_count').val(count);

});
//Add new Button

$("#addnew").click(function(){
  var count1=1;
$('#addnewdiv').show();

var limit=$('#total_count').val();

var totalcount=parseInt(limit)+parseInt(count1);

$('#total_count').val(totalcount);
$('#addnewdiv').append('<div class="row sharath" id="showDiv_'+totalcount+'"><div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-1 text-left "><div class="numbers"><label>'+totalcount+'</label></div></div><div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-9 text-left"><label for="serial" class="hidden-lg hidden-md">Serial Number <span class="needed">*</span> <span class="how"><a href="#" data-toggle="modal" data-target="#myModal" id="myBtn"><i class="fas fa-question-circle"></i></a></span></label><input type="text" class="form-control" id="serial_no" name="serial_no[]"  value="" ></div><div class="clearfix hidden-lg hidden-md hidden-sm"></div><div class="form-group hidden-lg hidden-md hidden-sm col-xs-1 text-center"></div><div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-9 "><label for="email" class="hidden-lg hidden-md">Country <span class="needed">*</span></label><select id="country" name="country[]" class="custom-select"  ><option value="">Select Country</option>@foreach($countries as $key=>$value)<option value="{{$value->COUNTRIES}}">{{$value->COUNTRIES}}</option>@endforeach</select></div><div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center"><div class="reset"><a href="#" id="delete_{{$i}}" name="{{$i}}" class="delebtn"><i class="far fa-times-circle fa-3x" aria-hidden="false"></i></a></div></div></div>');

//$('#total_count').val(count);
                                     
});
</script>
@endsection   